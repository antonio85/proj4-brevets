"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY#from credentials

###
# Pages
###
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    #app.logger.debug("Got a JSON request")
    #app.logger.debug("-----funciona------")
    #it gets the data from the webpage and call the fuctions to make the calculations.
    km = request.args.get('km', 999, type=float)#This block gets variables from the script in html
    dist_km = request.args.get('brevet_dist_km', 999, type=float)
    hour = request.args.get('begin_time')
    date = request.args.get('begin_date')
    tiempo_completo = "{} {}".format(date, hour)

    if dist_km > km:
        open_time = acp_times.open_time(km, dist_km, tiempo_completo)#send the parameter to the calculator.
        close_time = acp_times.close_time(km, dist_km, tiempo_completo)
        app.logger.debug("open={}".format(open_time))
        app.logger.debug("close={}".format(close_time))

        result = {"open": open_time, "close": close_time}#to js.
    else:#if location of brevets is out of the range from the track, returns the current date
        result = {"open": arrow.now().isoformat(), "close": arrow.now().isoformat()}#to js.

        
    return flask.jsonify(result=result)

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0",debug=True)
