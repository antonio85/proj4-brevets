
import arrow#for the hour
import math

distance = [0, 200,400,600,1000,1300, 30, 60]#distances, the last two are the special cases
speed_open = [34,32,30,28,26]#
speed_close = [15,11.428,13.333]

def split(distance, speed):#method that calculate the hours and minutes to add the the current time, according the speed.
    x = distance/float(speed)#time in decimal
    i,d  = math.modf(x)#separate the integer part from the decimal part.
    hours = round(d)
    minutes = round(60*i)
    return hours, minutes#Returns a tuple with the hours and the minutes.


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

    start = arrow.get(brevet_start_time)#get the hour from the parameters
    start = start.replace(tzinfo='US/Pacific')#shift the hours and adjust the time zone

    hour = 0
    minute = 0

    #if the track is longer than the entire path, the current hour would be returned as signal of error.
    if control_dist_km == distance[0]:#if zero, opening time is zero too.
        minute = distance[0]
        hour = distance[0]

    if control_dist_km > distance[0] and control_dist_km <= distance[1]:
    #in each "if" statement, the right amount of time is added to the total hours and minutes.
    #at the end
        hourTemp, minuteTemp = split(control_dist_km, speed_open[0])
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km > distance[1] and control_dist_km <=distance[2]:
        hourTemp, minuteTemp = split(distance[1], speed_open[0])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[1], speed_open[1])
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km > distance[2] and control_dist_km <=distance[3]:
        hourTemp, minuteTemp = split(distance[1], speed_open[0])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[1], speed_open[1])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[2], speed_open[2])
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km > distance[3] and control_dist_km <=distance[4]:
        hourTemp, minuteTemp = split(distance[1], speed_open[0])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[1], speed_open[1])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[1], speed_open[2])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[3], speed_open[3])
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km> distance[4] and control_dist_km <=distance[5]:
        hourTemp, minuteTemp = split(distance[1], speed_open[0])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[1], speed_open[1])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[1], speed_open[2])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[2], speed_open[3])
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[4], speed_open[4])
        hour = hour + hourTemp
        minute  = minute + minuteTemp

    start = start.shift(hours = hour, minutes = minute)

    return start.isoformat()#returns the opening time with format.



#same logic than open_time but using the slowest speed.
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):

    start = arrow.get(brevet_start_time)
    start = start.replace(tzinfo='US/Pacific')#correct the time.

    hour = 0
    minute = 0

    if control_dist_km == distance[0]:
        minute = distance[0]
        hour = 1 #the starting point(0km) close one hour after opened.

    if control_dist_km > distance[0] and control_dist_km <= distance[7]:#special rule for distance
        hourTemp, minuteTemp = split(control_dist_km, 20)
        hour = hour + hourTemp + 1#if the distance is less than 60, the speed used is 20km/h and we add 1 hour to this time.
        minute  = minute +minuteTemp

    if control_dist_km > distance[7] and control_dist_km <= distance[1]:
        hourTemp, minuteTemp = split(control_dist_km, 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km > distance[1] and control_dist_km <=distance[2]:
        hourTemp, minuteTemp = split(distance[1], 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[1], 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km > distance[2] and control_dist_km <=distance[3]:
        hourTemp, minuteTemp = split(distance[2], 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[2], 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km > distance[3] and control_dist_km <=distance[4]:
        hourTemp, minuteTemp = split(distance[3], 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km-distance[3], 11.428)
        hour = hour + hourTemp
        minute  = minute +minuteTemp

    if control_dist_km> distance[4] and control_dist_km <=distance[5]:
        hourTemp, minuteTemp = split(distance[3], 15)
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(distance[2], 11.428)
        hour = hour + hourTemp
        minute  = minute +minuteTemp
        hourTemp, minuteTemp = split(control_dist_km - distance[4], 13.333)
        hour = hour + hourTemp
        minute  = minute + minuteTemp

    start = start.shift(hours = hour, minutes = minute)

    return start.isoformat()
