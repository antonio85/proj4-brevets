from acp_times import open_time, close_time

import arrow
import nose

start = "2020-05-15 00:00"
#Open time test
def test_open():
    assert open_time(0, 200,start ) == "2020-05-15T00:00:00-07:00"

def test_open_2():
    assert open_time(100, 200, start) == "2020-05-15T02:56:00-07:00"

def test_open3():
    assert open_time(500, 1000, start) == "2020-05-15T15:28:00-07:00"

#Close time test
def test_close1():
    assert close_time(0, 1000, start) == "2020-05-15T01:00:00-07:00"

def test_close2():
    assert close_time(100, 200, start) == "2020-05-15T06:40:00-07:00"

def test_close3():
    assert close_time(599, 1000, start) == "2020-05-16T15:56:00-07:00"
