Brevet calculator with Ajax.

Student: Antonio Jesus Silva Paucar
Class: 322 Spring 2020

The server will contain the logic for calculating the times when a control post would open and close in a bike race.  In order to start the server, a makefile have been provided. We only need to input the commnad "make" and it will compile and execute the server, being able to look for it in the browser at the address "localhost:5000", leaving it ready for use. Whenever there is a mistake with the data provided (like the control post is not inside the boundaries of the whole race), the current date will be showed as a signal of problem.
